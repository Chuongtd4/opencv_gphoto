QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

QMAKE_CXXFLAGS += -std=c++0x -pthread
LIBS += -pthread    -lrt    -lm
# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
QMAKE_LFLAGS += "-Werror -W -Wno-missing-field-initializers -Wall -std=c++11 -pipe -g "
# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
    source/ak/ak.cpp \
    source/ak/fsm.cpp \
    source/ak/message.cpp \
    source/ak/timer.cpp \
    source/ak/tsm.cpp \
    source/app/task/task.cpp \
    source/app/app.cpp \
    source/app/app_config.cpp \
    source/app/app_data.cpp \
    source/app/task_list.cpp \
    source/common/base64.cpp \
    source/common/cmd_line.cpp \
    source/common/fifo.cpp \
    source/common/firmware.cpp \
    source/common/jsmn.cpp \
    source/sys/sys_dbg.cpp \
    main.cpp \
    source/driver/gpio.c \
    source/driver/rs232.c

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

LIBS +=/usr/local/lib/*.so
INCLUDEPATH += /usr/local/include/opencv4/ \
              source/ak/ \
              source/app/ \
              source/app/task/ \
              source/common/ \
              source/driver/ \
              source/sys \


HEADERS += \
    source/ak/ak.h \
    source/ak/ak_dbg.h \
    source/ak/message.h \
    source/ak/port.h \
    source/ak/timer.h \
    source/app/task/task.h \
    source/app/app.h \
    source/app/app_config.h \
    source/app/app_data.h \
    source/app/app_dbg.h \
    source/app/task_list.h \
    source/common/base64.h \
    source/common/cmd_line.h \
    source/common/fifo.h \
    source/common/firmware.h \
    source/common/jsmn.h \
    source/common/json.hpp \
    source/driver/gpio.h \
    source/driver/rs232.h \
    source/sys/sys_boot.h \
    source/sys/sys_dbg.h
